#!/usr/bin/python3

# GNU AGPL 3.0

import json
import sys
import re

def filterregx(regx):
  def f(d):
    rx = re.compile(regx)
    d2 = dict()
    for k in d.keys():
      if not rx.match(k):
        d2[k] = list()
        for v in d[k]:
          if not rx.match(v):
            d2[k].append(v)
    return d2
  return f

if len(sys.argv) >= 3:
  filterfun = filterregx(sys.argv[2])
else:
  filterfun = lambda d: d


with open(sys.argv[1]) as f:
  d = filterfun(json.load(f))
  print('digraph D {')
  for v in d.keys():
    print('  "{}" [shape=box]'.format(v))
  print(' ')
  for v in d.keys():
    for w in d[v]:
      print('  "{}" -> "{}"'.format(v,w))
  print('}')
